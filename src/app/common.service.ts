import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';


@Injectable()
export class CommonService {

  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http.get('https://jsonplaceholder.typicode.com/users')
      .map(response => {
        const data: any = response;
        return data;
      });
    // .catch(this.errHandler.logAndThrow)
  }

  getAlbumsofUser() {
    return this.http.get('https://jsonplaceholder.typicode.com/albums')
      .map(response => {
        const data: any = response;
        return data;
      });
    // .catch(this.errHandler.logAndThrow)
  }

  getPhotosofUser(){
    return this.http.get('https://jsonplaceholder.typicode.com/photos')
      .map(response => {
        const data: any = response;
        return data;
      });

  }

}
