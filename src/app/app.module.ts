import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { HeaderComponent } from './header/header.component';
import { AlbumsComponent } from './albums/albums.component';
import { PicturesComponent } from './pictures/pictures.component';
const appRoutes: Routes = [
  { path: 'users', component: UsersComponent},
  { path: '', redirectTo: 'users', pathMatch: 'full' },
  {path: 'users/albums/:id', component: AlbumsComponent},
  {path: 'users/photos/:id', component: PicturesComponent}
 ];



@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    HeaderComponent,
    AlbumsComponent,
    PicturesComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule
  ],
  exports: [ RouterModule ],
  providers: [],
  bootstrap: [AppComponent]
})



export class AppModule { }
