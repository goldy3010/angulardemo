import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [CommonService]
})
export class UsersComponent implements OnInit {
  userList = [];

  constructor(private commonService: CommonService, private router: Router) { }

  ngOnInit() {
    this.commonService.getUsers()
      .subscribe(
        (data: any) => {
          console.log(data);
          if (data.length > 0) {
            this.userList = data;
          } else {
            alert('there is some issue with request');
          }

        }
      );
  }

  gotoUserAlbums(userId) {
    this.router.navigate(['users/albums/', userId ]);
  

  } 

}
