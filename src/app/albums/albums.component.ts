import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '../common.service';
import {Location} from '@angular/common';


@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css'],
  providers: [CommonService]
})
export class AlbumsComponent implements OnInit {
  userAlbums = [];
  userId;
  private sub: any;
  constructor(private location: Location,private activatedRoute: ActivatedRoute, private router: Router, private commonService: CommonService, private route: Router) {
    this.sub = this.activatedRoute.params.subscribe(params => {
      this.userId = parseInt(params['id']);
      console.log(this.userId);
    });
  }

  ngOnInit() {
    this.commonService.getAlbumsofUser()
      .subscribe(
        (data: any) => {
          if (data.length > 0) {
            //alert(this.userId);
            for(var i = 0; i < data.length; i++)
            {
              if(this.userId === data[i].userId){
                this.userAlbums.push(data[i]);
              }
            }
            console.log(this.userAlbums);
            
          } else {
            alert('there is some issue with request');

          }
        }

      );
  }


  gotoPhotos(albumId) {
    this.router.navigate(['users/photos/', albumId ]);
  } 

  gotoBack(){
    //
    this.location.back();
  }

}
