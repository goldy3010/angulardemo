import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '../common.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-pictures',
  templateUrl: './pictures.component.html',
  styleUrls: ['./pictures.component.css'],
  providers: [CommonService]
})
export class PicturesComponent implements OnInit {
  albumsPhotos = [];
  albumId;
  private sub: any;

  constructor(private location: Location, private activatedRoute: ActivatedRoute, private router: Router, private commonService: CommonService, private route: Router) { 
    this.sub = this.activatedRoute.params.subscribe(params => {
      this.albumId = parseInt(params['id']);
      console.log(this.albumId);
    });
  }

  ngOnInit() {
    this.commonService.getPhotosofUser()
      .subscribe(
        (data: any) => {
          if (data.length > 0) {
            //alert(this.userId);
            for(var i = 0; i < data.length; i++)
            {
              if(this.albumId === data[i].albumId){
                this.albumsPhotos.push(data[i]);
              }
            }
            console.log(this.albumsPhotos);
            
          } else {
            alert('there is some issue with request');

          }
        }

      );
  }

  gotoBack(){
    //
    this.location.back();
  }

}
